/*! AMnote app.js
* ================
* Main JS application file for AMnote account manager v2. This file
* should be included in main pages. It controls some layout
* options and implements exclusive AMnote plugins.
*
* @Author  Tieu An Tuan
* @Support <thanhbop>
* @Email   <nguyenminhtuan.dev@gmail.com>
* @version 1.0.1
* @repository https://gitlab.com/alvinnguyen562/am-web.git
*/

// Make sure jQuery has been loaded
if (typeof jQuery === 'undefined') {
    console.log('Notice: AMnote requires jQuery!!!')
    throw new Error('AMnote requires jQuery')
}

/**
 * Layout()
 * =========
 */
$(window).ready(function() {
    var main_nav = $('.main-nav').height();
    $('.am-background').css('margin-top', main_nav);
    $(window).resize(function() {
        var main_nav = $('.main-nav').height();
        $('.am-background').css('margin-top', main_nav);
    });
    $('.main-menu-btn').click(function() {
        var mainMenuBtn = $(this).outerHeight();
        $('.wrapper-main').css('margin-top', 45);
    });
    $('#main-menu li ul li a').click(function() {
        if ($('.wrapper-main').find('.ui-tabs-panel').length) {
            $('.am-background').hide();
        }
    });

    $('[data-toggle="tooltip"]').tooltip();
    
    var itaImgLink = "http://www.roemheld.de/IT/Data/Images/Address/Italien.gif";
    var engImgLink = "http://www.roemheld.de/IT/Data/Images/Address/Grossbritanien.gif";
    var deuImgLink = "http://www.roemheld.de/IT/Data/Images/Address/Deutschland.gif";
    var fraImgLink = "http://www.roemheld.de/IT/Data/Images/Address/Frankreich.gif";

    var imgBtnSel = $('#imgBtnSel');
    var imgBtnIta = $('#imgBtnIta');
    var imgBtnEng = $('#imgBtnEng');
    var imgBtnDeu = $('#imgBtnDeu');
    var imgBtnFra = $('#imgBtnFra');

    var imgNavSel = $('#imgNavSel');
    var imgNavIta = $('#imgNavIta');
    var imgNavEng = $('#imgNavEng');
    var imgNavDeu = $('#imgNavDeu');
    var imgNavFra = $('#imgNavFra');

    var spanNavSel = $('#lanNavSel');
    var spanBtnSel = $('#lanBtnSel');

    imgBtnSel.attr("src", itaImgLink);
    imgBtnIta.attr("src", itaImgLink);
    imgBtnEng.attr("src", engImgLink);
    imgBtnDeu.attr("src", deuImgLink);
    imgBtnFra.attr("src", fraImgLink);

    imgNavSel.attr("src", itaImgLink);
    imgNavIta.attr("src", itaImgLink);
    imgNavEng.attr("src", engImgLink);
    imgNavDeu.attr("src", deuImgLink);
    imgNavFra.attr("src", fraImgLink);

    $(".language").on("click", function(event) {
        var currentId = $(this).attr('id');

        if (currentId == "navIta") {
            imgNavSel.attr("src", itaImgLink);
            spanNavSel.text("ITA");
        } else if (currentId == "navEng") {
            imgNavSel.attr("src", engImgLink);
            spanNavSel.text("ENG");
        } else if (currentId == "navDeu") {
            imgNavSel.attr("src", deuImgLink);
            spanNavSel.text("DEU");
        } else if (currentId == "navFra") {
            imgNavSel.attr("src", fraImgLink);
            spanNavSel.text("FRA");
        }

        if (currentId == "btnIta") {
            imgBtnSel.attr("src", itaImgLink);
            spanBtnSel.text("ITA");
        } else if (currentId == "btnEng") {
            imgBtnSel.attr("src", engImgLink);
            spanBtnSel.text("ENG");
        } else if (currentId == "btnDeu") {
            imgBtnSel.attr("src", deuImgLink);
            spanBtnSel.text("DEU");
        } else if (currentId == "btnFra") {
            imgBtnSel.attr("src", fraImgLink);
            spanBtnSel.text("FRA");
        }
    })

})

/**
 * Init main menu.
 * ===============
 */
+function($) {
    $mainMenu = $('#main-menu');
    $selector = {
        mainMenu : '#main-menu',
        dropdownNav : '.dropdown-nav',
    }
    $mainMenu.smartmenus({
        subMenusSubOffsetX: 6,
        subMenusSubOffsetY: -8,
        noMouseOver: true,
    });

    // SmartMenus mobile menu toggle button
    var $mainMenuState = $('#main-menu-state');
    if ($mainMenuState.length) {
        // animate mobile menu
        $mainMenuState.change(function(e) {
            var $menu = $mainMenu;
            if (this.checked) {
                $menu.hide().slideDown(250, function() {
                    $menu.css('display', '');
                });
            } else {
                $menu.show().slideUp(250, function() {
                    $menu.css('display', '');
                });
            }
        });
        // hide mobile menu beforeunload
        $(window).bind('beforeunload unload', function() {
            if ($mainMenuState[0].checked) {
                $mainMenuState[0].click();
            }
        });
    }

    // Slide up & slide down when click menu.
    $($selector.mainMenu + '>li').click(function(event) {
        if ($(this).children($selector.dropdownNav).is(':visible')) {
            $(this).children($selector.dropdownNav).slideUp()
        } else if ($(this).siblings().children($selector.dropdownNav).is(':visible')) {
            $(this).siblings().children($selector.dropdownNav).slideUp('slow')
            $(this).children($selector.dropdownNav).slideDown('fast')
        } else {
            $(this).children($selector.dropdownNav).slideDown('fast')
        }
    })
    $($selector.dropdownNav +' a').click(function(e) {
        e.preventDefault();
        $($selector.dropdownNav).slideUp('fast')
    })
    $(window).click(function() {
        $($selector.dropdownNav).hide('fast')
    });
    $mainMenu.click(function(e) {
        e.stopPropagation();
    })
}(jQuery)

/**
 * Init tabs jquery ui.
 * ====================
 */
+function($) {
    $navTabs = $("#nav-tabs");
    /* Create new tab function */
    var $tabs = $navTabs.tabs({
        beforeLoad: function(event, ui) {
            if (ui.tab.data("loaded")) {
                event.preventDefault();
                return;
            }
            ui.jqXHR.success(function() {
                ui.tab.data("loaded", true);
            });
            $('.tab-loading').removeClass('hidden');
        },
        load: function(e, ui) {
            $('.tab-loading').addClass('hidden');
        }
    });

    $navTabs.tabs()
        .on("click", '[role="tab"]', function() {
            $(this).closest("ul"); // The current UL
            var link = $(this).data('link');
        });

    $('.add-tab').click(function(e) {
        e.preventDefault();
        $('#main-logo').addClass('hidden');
        $('#loader').removeClass('hidden');
        $navTabs.removeClass('hidden');

        var tabName = $(this).text(),
            tabLink = $(this).attr('href'),
            tabNumber = -1;

        $tabs.find('.nav-tab-menu li a').each(function(i) {
            if ($(this).text() == tabName) {
                tabNumber = i;
            }
        });

        if (tabNumber >= 0) {
            $tabs.tabs('option', 'active', tabNumber)
        } else {

            var link = tabLink.split("/");
            var tempLink = "";
            for (var i = 3; i < link.length; i++) {
                tempLink += '/' + link[i];
            }
            $("<li data-link=" + tempLink + "><b><a  href=" + tabLink + ">" + tabName + "</a></b><span class='ui-icon ui-icon-close'></span></li>").appendTo(".nav-tab-menu");
            $navTabs.tabs("refresh");
            $navTabs.tabs('option', 'active', -1);
        }
        return false;
    });

    // Delete tabs.
    $(document).on('click', '.ui-icon-close', function(event) {
        var parent = $(this).parent(),
            index = parent.index(),
            tabs = $(this).closest(".ui-tabs");
        panel = tabs.children().eq(index + 1);
        parent.remove();
        panel.remove();
        tabs.tabs("refresh").tabs('option', 'active', -1);
        if ($('.nav-tab-menu li').length == 0) {
            $('.am-background').show();
            $navTabs.addClass('hidden');
        }
        /*init button*/
    });
}(jQuery)


    
